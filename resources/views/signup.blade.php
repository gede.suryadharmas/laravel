<!DOCTYPE html>
<html>
    <title>Form Sign Up</title>

    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
            <form>
                <label for="fname">First name:</label><br><br>

                    <input type="text" id="fname" name="fname"><br><br>
 
                <label for="lname">Last name:</label><br><br>

                    <input type="text" id="lname" name="lname"><br><br>
                <label for="gender">Gender:</label><br><br>

                    <input type="radio" id="male" name="gender" value="male">
                    <label for="male">Male</label><br>
                    <input type="radio" id="female" name="gender" value="female">
                    <label for="female">Female</label><br>
                    <input type="radio" id="other" name="gender" value="other">
                    <label for="other">Other</label><br><br>

                <label for="nationality">Nationality:</label><br><br>
                    <select id="Nationality" name="Nationality">
                        <option value="english">English</option>
                        <option value="indonesia">Indonesian</option>
                        <option value="japanese">Japanese</option>
                        <option value="mexicans">Mexicans</option>
                    </select><br><br>

                <label for="bahasa">Language Spoken:</label><br><br>
                    <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia">
                    <label for="vehicle1"> Bahasa Indonesia</label><br>
                    <input type="checkbox" id="bahasa2" name="bahasa2" value="English">
                    <label for="vehicle2"> English</label><br>
                    <input type="checkbox" id="bahasa3" name="bahasa3" value="Other">
                    <label for="vehicle3"> Other</label><br><br>

                <label for="biodata">Bio:</label><br><br>
                    <textarea name="biodata1" rows="10" cols="30"></textarea><br>                
            </form>
                <a href="/welcome">
                    <button>Sign Up</button>
                </a>
    </body>
</html>